from __future__ import print_function
import httplib2
import sys,os

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

import datetime

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

SCOPES = 'https://www.googleapis.com/auth/calendar'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Google Calendar API Python Quickstart'

def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    script_path =os.path.dirname(os.path.realpath(__file__))
    credential_dir = os.path.join(script_path, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'calendar-python-quickstart.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials

def callback(request_id, response, exception):
    if exception:
        # Handle error
        print (exception)
    else:
        print ("Response: %s" % response)


def main():
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('calendar', 'v3', http=http)

    now = datetime.datetime.fromtimestamp(0).isoformat() + 'Z' # 'Z' indicates UTC time
    eventsResult = service.events().list(
        calendarId='primary', timeMin=now, singleEvents=True, orderBy='startTime').execute()
    events = eventsResult.get('items', [])
    for n in range(0,len(events),100):
        batch = service.new_batch_http_request(callback=callback)
        for q in range(n,min(len(events),n+100)):
            batch.add(service.events().delete(calendarId='primary', eventId=events[q]['id']))
        print(n,min(len(events),n+100))
        batch.execute()
#        service.events().delete(calendarId='primary', eventId=event['id']).execute()

if __name__ == '__main__':
    main()

