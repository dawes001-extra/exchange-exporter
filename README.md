Exchange Exporter
-----------------
-----------------

This collection of tools is built to allow for the free export of calendar events stored in a Microsoft Exchange server out to a Google calendar.

To get it to work, first construct a virtualenv, then install the contents of requirements.txt.

Next, get API access to a google calendar as per the API access method. You will need to request an OAuth2 token. Once done, dump the json into this directory as the file 'client-secret.json'. The script will do the rest of the work for the flow, as per the Google Calendar API example.

Lastly, copy config.cfg.example to config.cfg and edit to your requirements. This is explicitly built to not store any passwords in plaintext.

The exchange_engine.py script will operate as an asynchronous executor, allowing for automated scheduling of exports. It will also prompt for a password on start, allowing it to keep that information in memory only.

