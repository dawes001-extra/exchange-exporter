from subprocess import Popen, PIPE
import sys, os, getpass, time
import time, threading

from configparser import ConfigParser
basepath =os.path.dirname(os.path.realpath(__file__))
config = ConfigParser()
config.read(basepath+os.sep+'config.cfg')

awaytime=config.getint('schedule','away_frequency')
exporttime=config.getint('schedule','export_frequency')

mypass = getpass.getpass('Password ')

env = os.environ
env['EXCHANGE_PASS'] = mypass

def do_away():
    p = Popen(['python','make_me_away.py'],env=env)
    p.communicate()
    threading.Timer(awaytime, do_away).start()

def do_export():
    p = Popen(['python','export_exchange_to_google.py'],env=env)
    p.communicate()
    threading.Timer(exporttime, do_export).start()

do_away()
do_export()
