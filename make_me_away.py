from __future__ import print_function
import httplib2
import sys,os
import base64
import pytz
import datetime
import traceback
import multiprocessing
import random
import re
import time

import vobject

from getpass import getpass
from datetime import timedelta
from exchangelib import DELEGATE, IMPERSONATION, Account, Credentials, ServiceAccount, \
    EWSDateTime, EWSTimeZone, Configuration, NTLM, CalendarItem, Message, \
    Mailbox, Attendee, Q, ExtendedProperty, FileAttachment, ItemAttachment, \
    HTMLBody, Build, Version, OofSettings

from configparser import ConfigParser
basepath =os.path.dirname(os.path.realpath(__file__))
config = ConfigParser()
config.read(basepath+os.sep+'config.cfg')

tz=config.get('main','timezone')
ETZ = EWSTimeZone.timezone(tz)

try:
    away_offset=config.getint('schedule','away_frequency')
except:
    away_offset=0

euser=config.get('exchange','username')
exchange_mail=config.get('exchange','mail')

if 'EXCHANGE_PASS' in os.environ:
    mypass = os.environ['EXCHANGE_PASS']
else:
    mypass = getpass('Password: ')

def get_exchange():
    credentials = ServiceAccount(username=euser, password=mypass)
    return Account(primary_smtp_address=exchange_mail, credentials=credentials,autodiscover=True, access_type=DELEGATE)

epoch = EWSDateTime.utcfromtimestamp(0)
now = ETZ.localize(EWSDateTime.now())+timedelta(seconds=away_offset)
future = now + timedelta(days=365)
past = now - timedelta(days=365)

account = get_exchange()
oof = account.oof_settings
filt = account.calendar.filter(start__range=(past, now),end__range=(now,future)).only('mime_content')
aways=[]
for event in filt:
    try:
        cal = vobject.readOne(event.mime_content.decode("utf-8"))
    except:
        print(event)
        print(traceback.format_exc())
        continue
    awayness = cal.vevent.contents['x-microsoft-cdo-busystatus'][0].value
     #May be one of ['FREE', 'TENTATIVE', 'BUSY', 'OOF']
    if awayness == 'OOF':
        start = ETZ.localize(EWSDateTime(*cal.vevent.contents['dtstart'][0].value.timetuple()[:7])).astimezone(EWSTimeZone.timezone('UTC'))
#        start = now.astimezone(EWSTimeZone.timezone('UTC'))
        end = ETZ.localize(EWSDateTime(*cal.vevent.contents['dtend'][0].value.timetuple()[:7])).astimezone(EWSTimeZone.timezone('UTC'))
        aways.append((start,end))
if len(aways)>0:
    for away in aways:
        if now - away[0] > now - aways[0][0]:
            aways[0][0]=away[0]
        if away[1] - now > aways[0][1] - now:
            aways[0][1]=away[1]
    start,end = aways[0]
    oof.state = OofSettings.SCHEDULED
    oof.start = start.astimezone(EWSTimeZone.timezone('UTC'))
    oof.end = end.astimezone(EWSTimeZone.timezone('UTC'))
    print('Setting as OOF for %s to %s' %(start, end))
    account.oof_settings = oof
