from __future__ import print_function
import httplib2
import sys,os
import base64
import pytz
import datetime
import traceback
import multiprocessing
import random
import re
import time

import vobject

from getpass import getpass
from datetime import timedelta
from exchangelib import DELEGATE, IMPERSONATION, Account, Credentials, ServiceAccount, \
    EWSDateTime, EWSTimeZone, Configuration, NTLM, CalendarItem, Message, \
    Mailbox, Attendee, Q, ExtendedProperty, FileAttachment, ItemAttachment, \
    HTMLBody, Build, Version

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

import googleapiclient.errors as gerrors

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

from configparser import ConfigParser
basepath =os.path.dirname(os.path.realpath(__file__))
config = ConfigParser()
config.read(basepath+os.sep+'config.cfg')

tz=config.get('main','timezone')
ETZ = EWSTimeZone.timezone(tz)

id_offset=config.getint('main','id_offset')

SCOPES=config.get('google','scopes')
CLIENT_SECRET_FILE=config.get('google','client_secret_file')
APPLICATION_NAME=config.get('google','application_name')
google_name=config.get('google','name')
google_mail=config.get('google','mail')

euser=config.get('exchange','username')
exchange_mail=config.get('exchange','mail')

nomail = 'no@example.com'

if 'EXCHANGE_PASS' in os.environ:
    mypass = os.environ['EXCHANGE_PASS']
else:
    mypass = getpass('Password: ')

def get_google_credentials():
    script_path =os.path.dirname(os.path.realpath(__file__))
    credential_dir = os.path.join(script_path, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'calendar-python-quickstart.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials


def get_exchange():
    credentials = ServiceAccount(username=euser, password=mypass)
    return Account(primary_smtp_address=exchange_mail, credentials=credentials,autodiscover=True, access_type=DELEGATE)


def create_export_event(event):
    try:
        cal = vobject.readOne(event.mime_content.decode("utf-8"))
    except:
        print(event)
        print(traceback.format_exc())
        return None

    organizer = {'displayName':event.organizer.name,'email':event.organizer.email_address}
    if organizer['email'] == exchange_mail:
        organizer['displayName'] = google_name
        organizer['email'] = google_mail
    else:
        organizer['email'] = nomail

    attendees = []
    if event.required_attendees:
        for a in event.required_attendees:
            attendees.append({'displayName':a.mailbox.name,'email':a.mailbox.email_address})
    if event.optional_attendees:
        for a in event.optional_attendees:
            attendees.append({'displayName':a.mailbox.name,'email':a.mailbox.email_address})
    for n,a in enumerate(attendees):
        if a['email'] == exchange_mail:
            a['displayName'] = google_name
            a['email'] = google_mail
        else:
            a['email'] = nomail
        attendees[n]=a
    myid={'displayName':google_name,'email':google_mail}
    if myid not in attendees:
        attendees.append(myid)
    if 'valarm' in cal.vevent.__dict__:
        reminder_minutes = -cal.vevent.valarm.trigger.value.total_seconds()/60
    else:
        reminder_minutes=15.0

    id = re.sub('[\W_-]+','',cal.vevent.uid.value)
#    missing_padding = len(id) % 4
#    if missing_padding != 0:
#        id += '='* (4 - missing_padding)
    id += "=" * (- len(id) % 4)
    try:
        uid=hex(int.from_bytes(base64.urlsafe_b64decode(id),'big')+id_offset)[2:]
    except:
        print(id)
        uid=str(id_offset+random.randrange(0,2**32))
#        uid = hex(int(id,16)+id_offset)[2:]
    gevent = {
      'summary': cal.vevent.summary.valueRepr(),
      'visibility': 'private',#getattr(cal.vevent,'class').value.lower(),
      'sequence': cal.vevent.sequence.value,
      'status': cal.vevent.status.value.lower(),
      'reminders':{ 'overrrides':
        [{'method':'popup',
         'minutes': reminder_minutes }
        ]
      },
      'organizer': organizer,
      'attendees': attendees,
      'iCalUID': uid,
    }

    if 'description' in cal.vevent.contents:
       gevent['description'] = cal.vevent.contents['description'][0].value
    if 'location' in cal.vevent.contents:
       gevent['location'] = cal.vevent.contents['location'][0].value

    start = cal.vevent.dtstart.value.isoformat()
    end = cal.vevent.dtend.value.isoformat()
    if len(start)<=10: #It's an all-day event
        gevent['start'] = {'date': cal.vevent.dtstart.value.isoformat(),'timeZone':tz},
        gevent['end'] = {'date': cal.vevent.dtend.value.isoformat(),'timeZone':tz},
    else:
        gevent['start'] = {'dateTime': cal.vevent.dtstart.value.isoformat(),'timeZone':tz},
        gevent['end'] = {'dateTime': cal.vevent.dtend.value.isoformat(),'timeZone':tz},

    recurrence=[]
    for rule in ['rrule','exrule','rdate','exdate']:
        if rule in cal.vevent.contents:
            for q in cal.vevent.contents[rule]:
                recurrence.append(q.serialize())
    if recurrence != []:
        gevent['recurrence']=recurrence
    #print(gevent)
    return gevent

def do_single_import(gevent, delay=0):
    if gevent is None: return
    time.sleep(delay)
    try:
        #print('Single Import: %s' % gevent)
        imported_event = service.events().import_(calendarId='primary', body=gevent).execute()
    except gerrors.HttpError as e:
        print(traceback.format_exc())
        if 'sequence' in e.args[0]:
            gevent['sequence'] = str(int(gevent['sequence'])+1)
            do_single_import(gevent)
        if 'Rate limit' in e.args[0]:
            do_single_imprt(gevent,(delay*10+1)**2/10)

def callback(request_id, response, exception):
    global event_cache
    if exception:
        print (exception)
        if 'sequence' in exception.args[0]:
            gevent = event_cache[int(request_id)]
            gevent['sequence'] = str(int(gevent['sequence'])+1)
            do_single_import(gevent)
    else:
#        print ("Response: %s" % response)
        event_cache[int(request_id)]=None

event_cache=[]
def do_import(gevents):
    global event_cache
    for n in range(0,len(gevents),100):
        batch = service.new_batch_http_request(callback=callback)
        for q in range(n,min(len(gevents),n+100)):
            if gevents[q] is not None:
                batch.add(service.events().import_(calendarId='primary', body=gevents[q]),request_id=str(len(event_cache)))
                event_cache.append(gevents[q])
        #print(n,min(len(gevents),n+100))
        batch.execute()

#    try:
#        imported_event = service.events().import_(calendarId='primary', body=gevent).execute()

def do_export_fragment(q):
    try:
        account = get_exchange()
        filt = account.calendar.all()[q:q+10]
        for x in filt:
            export_event(x)
        return True
    except:
        print(traceback.format_exc())
        return False

if __name__=='__main__':
    credentials = get_google_credentials()
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('calendar', 'v3', http=http)

    account = get_exchange()
    count = account.calendar.all().count()

#Subfilter like this, e.g.
#filt = account.calendar.filter(subject__contains='A SUBJECT')
#filt = account.calendar.filter(start__range=(
#    ETZ.localize(EWSDateTime(2017, 11, 1)),
#    ETZ.localize(EWSDateTime(2018, 1, 1))
#))
#    slices = range(0,count,100)
#    for slice in slices:
    filt = account.calendar.all().order_by('start').only('organizer','required_attendees','optional_attendees','mime_content')
#    filt = account.calendar.filter(start__range=(
#        ETZ.localize(EWSDateTime(2016, 6, 1)),
#        ETZ.localize(EWSDateTime(2018, 1, 1))
#        )).order_by('start').only('organizer','required_attendees','optional_attendees','mime_content')
#        filt = account.calendar.all().only('organizer','required_attendees','optional_attendees','mime_content')[slice:min(count,slice+100)]
    events = list(map(create_export_event,filt))
    do_import(events)
#    for x in filt:
#        create_export_event(x)
    event_cache = filter(bool,event_cache)
    for event in event_cache:
        do_single_import(event)
    exit()
    print(count)
    results=[False]
#    pool = multiprocessing.Pool(processes=1)
#    while any(x is False for x in results):
#        results=[]
 #       for slice in slices:
 #           results.append(do_export_fragment(slice))
##        results = pool.map(do_export_fragment,slices)
#        print(results)
#        slices = [x[0] for x in zip(slices,results) if x[1]]
